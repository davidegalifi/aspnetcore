﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace WebApp.Services
{
    public interface IGreeterService
    {
        string GetGreeting();
    }
    
    public class GreeterService : IGreeterService
    {
        private readonly string _greeting;

        public GreeterService(IConfiguration configuration)
        {
            _greeting = configuration["greetingFromSrvc"];
        }
        public string GetGreeting()
        {
            return _greeting;
        }
    }
}
